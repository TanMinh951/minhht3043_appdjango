from django.shortcuts import render
from .models import Rental
from .rentalFilter import rentalFilter
from rest_framework import status, viewsets
from .serializer import RentalSerializer

# Create your views here.


class RentalViewset(viewsets.ModelViewSet):
    queryset = Rental.objects.all()
    serializer_class = RentalSerializer
    filterset_class = rentalFilter


