from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import RentalViewset
from . import views

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
# from .views import index
router = DefaultRouter()
router.register('api', RentalViewset, basename="rentalviewset")

urlpatterns = [
    path('', include(router.urls)),
]