from .models import Rental, Inventory,Address, Store, Staff, Customer, Film
from rest_framework import serializers

from django.contrib.auth import get_user_model
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')
class FilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Film
        fields ='__all__'

class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['staff_id',"first_name",'last_name', 'email','active','username']
        
class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'

class StoreSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    manager_staff =StaffSerializer()
    class Meta:
        model = Store
        fields = ['store_id', 'address','manager_staff','last_update']     

class InventorySerializer(serializers.ModelSerializer):
    store = StoreSerializer()
    film = FilmSerializer()
    class Meta:
        model = Inventory
        fields = ['inventory_id', 'film','store','last_update']

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'

class RentalSerializer(serializers.ModelSerializer):
    full_info = serializers.SerializerMethodField()
    class Meta:
        model = Rental
        fields =['rental_id','rental_date','customer','inventory','return_date','last_update','staff', 'full_info']
    def get_full_info(self, obj):
        inventory_serializer = InventorySerializer(obj.inventory)
        customer_serializer = CustomerSerializer(obj.customer)
        staff_serializer = StaffSerializer(obj.staff)
        return {
            'customer': customer_serializer.data,
            'inventory': inventory_serializer.data,
            'staff': staff_serializer.data,
       }


# class RentalSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Rental
#         fields = "__all__"
