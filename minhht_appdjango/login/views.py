from django.shortcuts import render
from rest_framework import status, viewsets
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
import requests
from .serializer import UserSerializer
import jwt
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny

# Create your views here.

class UserViewset(viewsets.ModelViewSet):
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializer

@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    username = request.data['username']
    password = request.data['password']
    message = {}
    try:
        token_api = "http://127.0.0.1:8000/login/api/token/"
        query = {
            "username": username,
            "password": password
        }
        response_token = requests.post(url=token_api, data= query)
    except:
        message['error'] = "Có lỗi xảy ra vui lòng try cập lại sau"
        return Response(message, status=status.HTTP_400_BAD_REQUEST)

    if response_token.status_code <600 and response_token.status_code> 300:
        message['error'] = "Tài khoản hoặc mật khẩu không chính xát"
        return Response(message, status=status.HTTP_400_BAD_REQUEST)
   
    user = User.objects.get(username=username)
    serializer = UserSerializer(user)
    data={}
    token = response_token.json()
    data['token'] = token
    data['user'] = serializer.data
    return Response(data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    username = request.data['username']
    password = request.data['password']
    email = request.data['email']
    message = {}
    try:
        token_ad_api = "http://127.0.0.1:8000/login/api/token/"
        query_ad = {
            "username": 'admin',
            "password": 'zxcvb12345'
        }
        response_token = requests.post(url=token_ad_api, data= query_ad)
    except:
        message['error'] = "Có lỗi xảy ra vui lòng try cập lại sau"
        return Response(message, status=status.HTTP_400_BAD_REQUEST)

    token_ad = response_token.json()['access']
    print(token_ad)
    try:
        user_api = "http://127.0.0.1:8000/login/api/user/"
        query_user= {
            "username": username,
            "password": password,
            "email":email,
            "is_active": True,
        }

        header = {
            "Authorization": "Bearer "+ token_ad,
        }
        response_user = requests.post(url=user_api, data= query_user, headers=header)
    except:
        message['error'] = "Có lỗi xảy ra vui lòng try cập lại sau"
        return Response(message, status=status.HTTP_400_BAD_REQUEST)

    data = {}
    # user = response_user.json()
    # data['user'] = user
    # if  response_user.status_code >399:
    #     return Response(data, status=status.HTTP_400_BAD_REQUEST)
    
    # try:
    #     token_api = "http://127.0.0.1:8000/login/api/token/"
    #     query = {
    #         "username": username,
    #         "password": password
    #     }
    #     response_token = requests.post(url=token_api, data= query)
    # except:
    #     message['error'] = "Có lỗi xảy ra vui lòng try cập lại sau"
    #     return Response(message, status=status.HTTP_400_BAD_REQUEST)
    
    # token = response_token.json()
    # data['token']= token
    data['message'] = "Đăng kí tài khoản thành công!"
    return Response(data, status=status.HTTP_201_CREATED)