from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import UserViewset, login
from . import views


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
# from .views import index
router = DefaultRouter()
router.register('', UserViewset, basename="userviewset")

urlpatterns = [
    path('api/user/', include(router.urls)),
    path('api/login/', views.login),
    path('api/register/', views.register),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]